﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace syscon.Parus8.Server
{
	partial class RoleFunctions
	{
		[Remote(IsPure = true)]
		public override Sungero.Company.IEmployee GetRolePerformer(Sungero.Docflow.IApprovalTask task)
		{
			var mainTask = syscon.SC.ApprovalTasks.As(task);
			
			if (_obj.Type == syscon.Parus8.Role.Type.DocRegister)
			{
				var regGroup = mainTask.RegGroupKS;		
				return regGroup.ResponsibleEmployee;
			}
			
			foreach (var role in mainTask.RolesSysconsyscon)
			{
				if (_obj.Type == role.RoleProperty.Type)
				{
					return role.EmpProperty;
				}
			}
					
//			if (_obj.Type == syscon.Parus8.Role.Type.DivisionHead)
//				return mainTask.DivisionHeadsyscon;
//			
//			if (_obj.Type == syscon.Parus8.Role.Type.HeadSecurity)
//				return mainTask.HeadSecurityKS;
//			
//			if (_obj.Type == syscon.Parus8.Role.Type.Accountant)
//				return mainTask.AccountantKS;
//			
//			if (_obj.Type == syscon.Parus8.Role.Type.LegalDep)
//				return mainTask.LegalDepKS;
//			
//			if (_obj.Type == syscon.Parus8.Role.Type.PersonnelDel)
//				return mainTask.PersonnelDepKS;
			
			return base.GetRolePerformer(task);
		}
		
	}
}
