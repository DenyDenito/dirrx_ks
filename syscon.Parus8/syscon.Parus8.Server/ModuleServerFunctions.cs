﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Contracts;
using Sungero.Company;
using Sungero.Parties;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Sungero.FinancialArchive;
using System.Data;
using System.IO;
using Microsoft.Office.Interop.Excel;

namespace syscon.Parus8.Server
{
	public class ModuleFunctions
	{


		[Remote, Public]
		public void ImportContractCatFromExcel()
		{
			var file = @"C:\1.xlsx";
			Microsoft.Office.Interop.Excel.Application exel = new Microsoft.Office.Interop.Excel.Application();
			Microsoft.Office.Interop.Excel.Workbook wb = exel.Workbooks.Open(file);
			Microsoft.Office.Interop.Excel.Worksheet sh = (Microsoft.Office.Interop.Excel.Worksheet)wb.Sheets.get_Item(1);
			Microsoft.Office.Interop.Excel.Range data= sh.UsedRange;
			for (int i = 2; i < 200; i++)
			{
				if (data.Cells[i,2].ToString() != null)
				{
					var cat = SC.ContractCategories.Create();
					cat.Name = data.Cells[i,1].ToString();
					cat.MainCategorysyscon = data.Cells[i,2].ToString();
					cat.Save();
				}
			}
		}
		/// <summary>
		/// Загрузка файла из Парус8
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static string DownloadFileParus8(string RN)
		{
			string filePath = string.Empty;
			
			OracleConnection connection = new OracleConnection();
			connection.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.69)(PORT=1521)))(CONNECT_DATA=(SID=kse)));User Id=directum;Password=$$$$$$aP@;";
			connection.Open();
			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandType = System.Data.CommandType.StoredProcedure;
				command.CommandText = "up_rx_get_blob";
				command.Parameters.Add("sRN", RN);
				command.Parameters.Add("sFileName", OracleDbType.Varchar2, ParameterDirection.Output);
				command.Parameters["sFileName"].Size = 240;
				command.Parameters.Add("bDate", OracleDbType.Blob, ParameterDirection.Output);
				command.ExecuteNonQuery();
				filePath = @"C:\BLOB\";
				filePath += command.Parameters["sFileName"].Value.ToString();
				var bDate = command.Parameters["bDate"].Value;

				using (System.IO.FileStream fs = new System.IO.FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write))
				{
					byte[] bytes = new byte[0];
					bytes = (byte[])((OracleBlob)bDate).Value;
					int ArraySize = new int();
					ArraySize = bytes.GetUpperBound(0);
					fs.Write(bytes, 0, ArraySize);
				}
			}
			
			return filePath;
		}
		
		/// <summary>
		/// Массовая загрузка организаций из Парус 8
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		[Remote, Public]
		public static List<string> GetChangedObjects_Parus8(int Type)
		{
			var ListOrgRN = new List<string>();
			
			OracleConnection connection = new OracleConnection();
			connection.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.67)(PORT=1521)))(CONNECT_DATA=(SID=kse)));User Id=directum;Password=$$$$$$aP@;";
			connection.Open();

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandText = string.Format("select RN_DOC from PARUS.UT_RX_NEWDOC where TYPE_DOC = {0}", Type.ToString());
				var reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						ListOrgRN.Add(reader["RN_DOC"].ToString());
					}
				}
			}
			
			return ListOrgRN;
		}
		
		/// <summary>
		/// Очистка таблицы измененных объектов
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static void RemoveChangedObject_Parus8(List<string> ListOrgRN)
		{
			OracleConnection connection = new OracleConnection();
			connection.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.67)(PORT=1521)))(CONNECT_DATA=(SID=kse)));User Id=directum;Password=$$$$$$aP@;";
			connection.Open();

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandText = string.Format("DELETE from PARUS.UT_RX_NEWDOC WHERE RN_DOC in {0}", string.Join(",", ListOrgRN));
				command.ExecuteNonQuery();
			}
			connection.Close();
		}
		
		/// <summary>
		/// Очистка таблицы измененных объектов
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static void RemoveChangedObject_Parus8(string RN)
		{
			OracleConnection connection = new OracleConnection();
			connection.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.67)(PORT=1521)))(CONNECT_DATA=(SID=kse)));User Id=directum;Password=$$$$$$aP@;";
			connection.Open();

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandText = string.Format("DELETE from PARUS.UT_RX_NEWDOC WHERE RN_DOC = {0}", RN);
				command.ExecuteNonQuery();
			}
			connection.Close();
		}
		
		/// <summary>
		/// Загрузка организаций из Парус 8
		/// </summary>
		[Remote, Public]
		public static void Sync_Organizations_RX(string RN)
		{
			OracleConnection connection = new OracleConnection();
			connection.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.67)(PORT=1521)))(CONNECT_DATA=(SID=kse)));User Id=directum;Password=$$$$$$aP@;";
			connection.Open();

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandText = string.Format("SELECT RN, AGNNAME, AGNIDNUMB, REASON_CODE FROM PARUS.AGNLIST WHERE RN = {0}", RN);
				var reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						var rn = reader["RN"].ToString();
						var nameOrg = reader["AGNNAME"].ToString();
						var inn = reader["AGNIDNUMB"].ToString();
						var kpp = reader["REASON_CODE"].ToString();

						Sync_Organization_RX(rn, nameOrg, inn, kpp);
					}
				}
			}
			
			connection.Close();

			//				RemoveChangedObject_Parus8(ListOrgRN);
		}
		
		/// <summary>
		/// Массовая загрузка организаций из Парус 8
		/// </summary>
		[Remote, Public]
		public static string MultipleSync_Organizations_RX()
		{
			var cntLoad = 0;
			var cntError = 0;
			var cntAll = 0;

			var logsFolder = @"C:\DRX\logs\";

			if (!Directory.Exists(logsFolder))
				Directory.CreateDirectory(logsFolder);

			// Передаем тип для возвращаемых объектов - 1 (Организации)
			var ListOrgRN = GetChangedObjects_Parus8(1);

			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

			if (ListOrgRN.Count > 0)
			{
				sw.Start();

				OracleConnection connection = new OracleConnection();
				connection.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.67)(PORT=1521)))(CONNECT_DATA=(SID=kse)));User Id=directum;Password=$$$$$$aP@;";
				connection.Open();

				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = connection;
					command.CommandText = "SELECT Org.RN, Org.AGNNAME, Org.AGNIDNUMB, Org.REASON_CODE FROM PARUS.AGNLIST Org INNER JOIN PARUS.UT_RX_NEWDOC Change ON Org.RN = Change.RN_DOC WHERE Change.TYPE_DOC = 1";
					var reader = command.ExecuteReader();
					if (reader.HasRows)
					{
						while (reader.Read())
						{
							var rn = reader["RN"].ToString();
							var nameOrg = reader["AGNNAME"].ToString();
							var inn = reader["AGNIDNUMB"].ToString();
							var kpp = reader["REASON_CODE"].ToString();

							try
							{
								Sync_Organization_RX(rn, nameOrg, inn, kpp);

								using (var file = File.AppendText(string.Format(logsFolder + "log_migration_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
								{
									file.WriteLine(string.Format("Организация {0} (RN {1}) загружена: INN {2}, KPP {3}", nameOrg, rn, inn, kpp));
								}

								cntLoad++;
							}
							catch (Exception ex)
							{
								using (var file = File.AppendText(string.Format(logsFolder + "log_error_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
								{
									file.WriteLine(string.Format("Организация c RN {0} не загружена загружена.\r\n{1}", rn, ex.Message));
								}

//								ListOrgRN.Remove(reader["RN"].ToString());
								cntError++;
							}
							cntAll++;
						}
					}
				}
				connection.Close();

				sw.Stop();
				//				RemoveChangedObject_Parus8(ListOrgRN);
			}

			string resultMessage = string.Format("Загрузка завершена.\r\n"
			                                     + "---------------------------------------------------------\r\n"
			                                     + "Количество загруженных организаций: {0}\r\n"
			                                     + "Количество ошибок при загрузке: {1}\r\n"
			                                     + "---------------------------------------------------------\r\n"
			                                     + "Количество просмотренных организаций: {2}\r\n"
			                                     + "---------------------------------------------------------\r\n"
			                                     + "Время выполнения: {3}",
			                                     cntLoad, cntError, cntAll,
			                                     sw.Elapsed.ToString(@"d\.hh\:mm\:ss"));
			//			                                     "0");

			sw = null;

			return resultMessage;
		}

		/// <summary>
		/// Создание или обновление документа входящего счета на оплату в системе Парус 8
		/// </summary>
		/// <param name="Document"></param>
		[Remote, Public]
		public static void Sync_IncomingInvoice_Parus8(IIncomingInvoice Document)
		{
			var documentRN = string.Empty;
			var contractRN = string.Empty;
			var counterpartyRN = string.Empty;
			var BusinessUnitRN = string.Empty;
			
			if (!TryRN_By_ID(Document.Contract.Id,out contractRN))
				contractRN = Sync_Contract_Parus8(Document.Contract);
			
			if (!TryRN_By_ID(Document.Counterparty.Id, out counterpartyRN))
				counterpartyRN = Sync_Organization_Parus8(Document.Counterparty);
			
			if (!TryRN_By_ID(Document.BusinessUnit.Id, out BusinessUnitRN))
				BusinessUnitRN  = Sync_Organization_Parus8(Document.BusinessUnit.Company);
			
//			Document.Id;
//			Document.Number;
//			Document.Date.ToString("d");
//			Document.TotalAmount.ToString();
			
			Sync_ExternalEntityLink(Document.Id, "0e9043d2-de08-44ac-af5d-b27dd79db385", documentRN, "Входящий счет на оплату");
		}
		

		/// <summary>
		/// Создание или обновление Договора в Парус 8
		/// </summary>
		/// <param name="Contract"></param>
		/// <returns></returns>
		public static string Sync_Contract_Parus8(Sungero.Docflow.IContractualDocumentBase Contract)
		{
			//В Парус 8 будет хранимая процедур, которая возвращает реквизиты контрагента по его RN
			return null;
		}
		
		/// <summary>
		/// Создание или обновление контрагента в Парус 8
		/// </summary>
		/// <param name="Counterpaty"></param>
		/// <returns>RN</returns>
		public static string Sync_Organization_Parus8(ICounterparty Counterpaty)
		{
			var RN = string.Empty;
			
			var name = string.Empty;
			var inn = string.Empty;
			var kpp = string.Empty;
			
			var company = Companies.As(Counterpaty);
			
			if (company != null)
			{
				name = company.Name;
				inn = company.TIN;
				kpp = company.TRRC;
			}
			else
			{
				name = Counterpaty.Name;
				inn = Counterpaty.TIN;
			}
			
			OracleConnection connection = new OracleConnection();
			connection.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.69)(PORT=1521)))(CONNECT_DATA=(SID=kse)));User Id=directum;Password=$$$$$$aP@;";
			connection.Open();

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandType = System.Data.CommandType.StoredProcedure;
				command.CommandText = "UP_RX_ADD_AGNLIST";
				
				command.Parameters.Add("sName", name);
				command.Parameters.Add("sINN", inn);
				command.Parameters.Add("sKPP", kpp);
				if (TryRN_By_ID(Counterpaty.Id, out RN))
					command.Parameters.Add("nRN", OracleDbType.Decimal, Decimal.Parse(RN), ParameterDirection.InputOutput);
				else
					command.Parameters.Add("nRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
				
				command.ExecuteNonQuery();
				
				RN = command.Parameters["nRN"].Value.ToString();
				
				Sync_ExternalEntityLink(company.Id, "593e143c-616c-4d95-9457-fd916c4aa7f8", RN, "Организация");
			}
			
			return RN;
		}
		
		/// <summary>
		/// Создания исходещей счет-фактуры
		/// </summary>
		/// <param name="RN">Идентификатор окумента в Парус 8</param>
		/// <param name="Num">Номер регистрации</param>
		/// <param name="DateDoc">Дата регистрации</param>
		/// <param name="OrgRN">Идентификатор контрагента в Парус 8</param>
		/// <param name="Sum">Сумма</param>
		/// <param name="OurOrgRN"></param>
		/// <param name="ContractRN"></param>
		/// <param name="FilePath"></param>
		/// <returns></returns>
		[Remote(IsPure = true)]
		public static IOutgoingTaxInvoice Sync_OutgoingTaxInvoice_RX(string RN, string Num, string DateDoc, string OrgRN, string Sum, string OurOrgRN, string ContractRN, string FilePath)
		{
			IOutgoingTaxInvoice taxInvoice = null;
			int ID;
			
			if (TryID_By_RN(RN, out ID)) //Если есть контрагент обновляем, иначе создаём нового
				taxInvoice = OutgoingTaxInvoices.Get(ID);
			else
				taxInvoice = OutgoingTaxInvoices.Create();
			
			taxInvoice.RegistrationNumber = Num;
			
			DateTime date;
			if (Calendar.TryParseDate(DateDoc, out date))
				taxInvoice.RegistrationDate =  date;
			
			int totalAmount;
			if (int.TryParse(Sum, out totalAmount))
				taxInvoice.TotalAmount = totalAmount;
			
			taxInvoice.Counterparty = GetOrg_By_RN(OrgRN);
			taxInvoice.BusinessUnit = GetOurOrg_By_OrgRN(OurOrgRN);
			taxInvoice.LeadingDocument = GetContract_By_RN(ContractRN);
			taxInvoice.Save();
			
			Sync_ExternalEntityLink(taxInvoice.Id, "ebf76282-a6e6-4bb0-93b0-06a6eb4853b8", RN, "Исходящая счет-фактура");
			
			return taxInvoice;
		}
		
		/// <summary>
		/// Создание или обновление Входящей счет-фактуры в Парус 8
		/// </summary>
		/// <param name="Document">Объект документа</param>
		/// <returns>RN</returns>
		public static string Sync_IncomingTaxInvoice_Parus8(IIncomingTaxInvoice Document)
		{
			string RN = string.Empty;
			
			var documentRN = string.Empty;
			var contractRN = string.Empty;
			var counterpartyRN = string.Empty;
			var businessUnitRN = string.Empty;
			
			if (!TryRN_By_ID(Document.LeadingDocument.Id,out contractRN))
				contractRN = Sync_Contract_Parus8(Document.LeadingDocument);
			
			if (!TryRN_By_ID(Document.Counterparty.Id, out counterpartyRN))
				counterpartyRN = Sync_Organization_Parus8(Document.Counterparty);
			
			if (!TryRN_By_ID(Document.BusinessUnit.Id, out businessUnitRN))
				businessUnitRN  = Sync_Organization_Parus8(Document.BusinessUnit.Company);
			
			OracleConnection connection = new OracleConnection();
			connection.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.69)(PORT=1521)))(CONNECT_DATA=(SID=kse)));User Id=directum;Password=$$$$$$aP@;";
			connection.Open();

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandType = System.Data.CommandType.StoredProcedure;
				command.CommandText = "XXX_XXX";
				command.Parameters.Add("sParam1", Document.RegistrationNumber);
				command.Parameters.Add("sParam2", Document.RegistrationDate.ToString());
//				command.Parameters.Add("sParam2", OracleDbType.Date, Document.RegistrationDate, ParameterDirection.Input);
				command.Parameters.Add("sParam3", counterpartyRN);
				command.Parameters.Add("sParam4", businessUnitRN);
				command.Parameters.Add("sParam5", contractRN);
				command.Parameters.Add("sParam6", Document.TotalAmount.ToString());
				command.Parameters.Add("nRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
				command.ExecuteNonQuery();
				
				RN = command.Parameters["nRN"].Value.ToString();
				
				Sync_ExternalEntityLink(Document.Id, "042a0bbc-88e9-49ed-887c-24fc30d1d4f3", RN, "Входящая счет-фактура");
			}
			
			return RN;
		}

		/// <summary>
		/// Создание контрагента (Create_ORG_RX)
		/// </summary>
		/// <param name="RN">RN контрагента</param>
		/// <param name="Name">Наименование контрагента</param>
		/// <param name="INN">ИНН контрагента</param>
		/// <param name="KPP">КПП контрагента</param>
		[Remote(IsPure = true)]
		public static Sungero.Parties.ICompany Sync_Organization_RX(string RN, string Name, string INN, string KPP)
		{
			ICompany org = null;
			int ID;
			
			if (TryID_By_RN(RN, out ID)) //Если есть контрагент обновляем, иначе создаём нового
				org = Companies.Get(ID);
			else
				org = Sungero.Parties.Companies.Create();
			
			org.Name = Name;
			org.TIN = INN;
			org.TRRC = KPP;
			org.Save();
			
			Sync_ExternalEntityLink(org.Id, "593e143c-616c-4d95-9457-fd916c4aa7f8", RN, "Организация");
			
			return org;
		}

		/// <summary>
		/// Создание договора (Create_Contract_RX)
		/// </summary>
		/// <param name="RN">RN договора</param>
		/// <param name="Category">Имя категории договора</param>
		/// <param name="RegNum">Рег №</param>
		/// <param name="DateDoc">Дата договора</param>
		/// <param name="Note">Содержание</param>
		/// <param name="Sum">Сумма</param>
		/// <param name="DateFrom">Действует С</param>
		/// <param name="DateTo">Действует По</param>
		/// <param name="ORG">RN контрагента</param>
		/// <param name="OurOrg">RN нашей организации</param>
		/// <param name="Signer">ИНН подписанта</param>
		/// <param name="Responsible">ИНН ответственного</param>
		/// <param name="File">Наличие тела документа</param>
		/// <param name="NeedOpenCard">Необходимость открытия карточки объекта после его создания</param>
		/// <returns></returns>
		[Remote(IsPure = true)]
		public static Sungero.Contracts.IContract Sync_Contract_RX(string RN, string Category, string RegNum, string DateDoc, string Note, string Sum, string DateFrom, string DateTo, string Org, string OurOrg, string Signer, string Responsible, string File)
		{
			IContract contract = null;
			int ID;
			
			if (TryID_By_RN(RN, out ID)) //Если есть договор обновляем, иначе создаём нового
			{
				contract = Sungero.Contracts.Contracts.Get(ID);
				
				contract.DocumentGroup = Create_Category(Category);
				contract.RegistrationNumber = RegNum;
				
				DateTime date;
				if (Calendar.TryParseDate(DateDoc, out date))
					contract.RegistrationDate =  date;
				
				DateTime date1;
				if (Calendar.TryParseDate(DateFrom, out date1))
					contract.ValidFrom = date1;
				
				DateTime date2;
				if (Calendar.TryParseDate(DateTo, out date2))
					contract.ValidTill = date2;
				
				contract.Subject = Note;
				
				double sum;
				if (Double.TryParse(Sum, out sum))
					contract.TotalAmount =  sum;
				
				contract.Counterparty = GetOrg_By_RN(Org);
				contract.BusinessUnit = GetOurOrg_By_OrgRN(OurOrg);
				
				var emps = syscon.SC.Employees.GetAll().FirstOrDefault(x => String.Equals(x.Person.TIN,Signer,StringComparison.CurrentCultureIgnoreCase));
				if (emps != null) //Если есть категория, возьмём её
				{
					contract.OurSignatory = emps;
				}
				
				emps = syscon.SC.Employees.GetAll().FirstOrDefault(x => String.Equals(x.Person.TIN,Responsible,StringComparison.CurrentCultureIgnoreCase));
				if (emps != null) //Если есть категория, возьмём её
				{
					contract.ResponsibleEmployee = emps;
					contract.Department = emps.Department;
				}
				
				contract.Save();
				Sync_ExternalEntityLink(contract.Id);
//				result = Sungero.Contracts.Contracts.Get(contract.Id);
			}
			else
			{
				contract = Sungero.Contracts.Contracts.Create();
				
				contract.DocumentGroup = Create_Category(Category);
				contract.RegistrationNumber = RegNum;
				
				DateTime date;
				if (Calendar.TryParseDate(DateDoc, out date))
					contract.RegistrationDate =  date;
				
				DateTime date1;
				if (Calendar.TryParseDate(DateFrom, out date1))
					contract.ValidFrom = date1;
				
				DateTime date2;
				if (Calendar.TryParseDate(DateTo, out date2))
					contract.ValidTill = date2;
				
				contract.Subject = Note;
				
				double sum;
				if (Double.TryParse(Sum, out sum))
					contract.TotalAmount =  sum;
				
				
				contract.Counterparty = GetOrg_By_RN(Org);
				contract.BusinessUnit = GetOurOrg_By_OrgRN(OurOrg);
				
				var emps = syscon.SC.Employees.GetAll().FirstOrDefault(x => String.Equals(x.Person.TIN,Signer,StringComparison.CurrentCultureIgnoreCase));
				if (emps != null) //Если есть категория, возьмём её
				{
					contract.OurSignatory = emps;
				}
				
				emps = syscon.SC.Employees.GetAll().FirstOrDefault(x => String.Equals(x.Person.TIN,Responsible,StringComparison.CurrentCultureIgnoreCase));
				if (emps != null) //Если есть категория, возьмём её
				{
					contract.ResponsibleEmployee = emps;
					contract.Department = emps.Department;
				}

				contract.Save();
				
				Sync_ExternalEntityLink(contract.Id, "f37c7e63-b134-4446-9b5b-f8811f6c9666", RN, "Договор");
//				write_time_sync(contract.Id);
//				result = Sungero.Contracts.Contracts.Get(contract.Id);
			}
			
			return contract;
		}
		
		/// <summary>
		/// Возвращает контрагента по его RN
		/// </summary>
		/// <param name="RN">RN контрагента</param>
		/// <returns></k>
		public static Sungero.Parties.ICompany GetOrg_By_RN(string RN)
		{
			int ID = ID_By_RN(RN);
			
			return Companies.Get(ID);
		}
		
		/// <summary>
		/// Возвращает контрагента по его RN
		/// </summary>
		/// <param name="RN">RN контрагента</param>
		/// <returns></k>
		public static IContract GetContract_By_RN(string RN)
		{
			int ID = ID_By_RN(RN);
			
			return Contracts.Get(ID);
		}

//		/// <summary>
//		/// Запрашивает данные контрагента по его RN из Парус 8
//		/// </summary>
//		/// <param name="RN">RN контрагента
//		/// </param>
//		/// <returns></returns>
//		public static Sungero.Parties.ICompany return_org_Parus8(string RN)
//		{
//			//В Парус 8 будет хранимая процедур, которая возвращает реквизиты контрагента по его RN
//			return null;
//		}

		/// <summary>
		/// Возвоазает ID по RN из таблицы ExternalLink
		/// </summary>
		/// <param name="RN"></param>
		/// <returns>ИД записи</returns>
		public static int ID_By_RN(string RN)
		{
			int result = 0;
			var links = Sungero.Commons.ExternalEntityLinks.GetAll().FirstOrDefault(x => String.Equals(x.ExtEntityId,RN));
			if (links != null)
			{
				result = Convert.ToInt32(links.EntityId);
			}
			return result;
		}

		/// <summary>
		/// Возвращает RN по ID из таблицы ExternalLink
		/// </summary>
		/// <param name="ID"></param>
		/// <returns> RN записи</returns>
		public static string RN_By_ID(int ID)
		{
			var result = string.Empty;
			var links = Sungero.Commons.ExternalEntityLinks.GetAll().FirstOrDefault(x => int.Equals(x.EntityId, ID));
			
			if (links != null)
			{
				result = links.ExtEntityId;
			}
			
			return result;
		}

		/// <summary>
		/// Возвоазает ID по RN из таблицы ExternalLink
		/// </summary>
		/// <param name="RN"></param>
		/// <param name="ID"></param>
		/// <returns></returns>
		private static bool TryID_By_RN(string RN, out int ID)
		{
			var result = true;
			ID = 0;
			var links = Sungero.Commons.ExternalEntityLinks.GetAll().FirstOrDefault(x => String.Equals(x.ExtEntityId,RN));
			
			if (links != null)
				ID = Convert.ToInt32(links.EntityId);
			else
				result = false;
			
			return result;
		}

		/// <summary>
		/// Возвращает RN по ID из таблицы ExternalLink
		/// </summary>
		/// <param name="ID"></param>
		/// <param name="RN"></param>
		/// <returns></returns>
		private static bool TryRN_By_ID(int ID, out string RN)
		{
			var result = true;
			RN = string.Empty;
			var links = Sungero.Commons.ExternalEntityLinks.GetAll().FirstOrDefault(x => int.Equals(x.EntityId, ID));
			
			if (links != null)
				RN = links.ExtEntityId;
			else
				result = false;
			
			return result;
		}

		/// <summary>
		/// Делает поиск нашей организации по организации
		/// </summary>
		/// <param name="ID">ИД организации</param>
		/// <returns>Ссылка на запись справочника "наши организации"</returns>
		public static Sungero.Company.IBusinessUnit GetOurOrg_By_OrgRN(string RN)
		{
			Sungero.Parties.ICompany company = GetOrg_By_RN(RN);
			
			return Sungero.Company.BusinessUnits.GetAll().FirstOrDefault(x => Equals(x.Company,company));
		}

		/// <summary>
		/// Создание и обновление связи объектов в ExternalEntityLink
		/// </summary>
		/// <param name="ID">ID синхронизируемого объекта</param>
		/// <param name="Type"></param>
		/// <param name="RN"></param>
		/// <param name="ExType"></param>
		public static void Sync_ExternalEntityLink(int ID, string Type, string RN, string ExType)
		{
			var link = Sungero.Commons.ExternalEntityLinks.GetAll().FirstOrDefault(x => x.EntityId.Equals(ID));
			if (link == null)
			{
				link = Sungero.Commons.ExternalEntityLinks.Create();
			}
			
			link.EntityType = Type;
			link.EntityId = ID;
			link.ExtEntityType = ExType;
			link.ExtEntityId = RN;
			
			link.SyncDate = Calendar.Now;
			link.Save();
		}

		/// <summary>
		/// Записывает в справочник ExternalEntityLink время синхронизации объекта
		/// </summary>
		/// <param name="ID">ID синхронизируемого объекта</param>
		public static void Sync_ExternalEntityLink(int ID)
		{
			var link = Sungero.Commons.ExternalEntityLinks.GetAll().FirstOrDefault(x => x.EntityId.Equals(ID));
			if (link != null)
			{
				link.SyncDate = Calendar.Now;
				link.Save();
			}
		}

		/// <summary>
		/// Вернуть или создать категорию договора при ее отсутствии
		/// </summary>
		/// <param name="Category"></param>
		/// <returns></returns>
		[Remote(IsPure = true)]
		public static Sungero.Contracts.IContractCategory Create_Category(string Category)
		{
			IContractCategory category = Sungero.Contracts.ContractCategories.GetAll().FirstOrDefault(x => String.Equals(x.Name,Category,StringComparison.CurrentCultureIgnoreCase));
			if (category != null) //Если есть категория, возьмём её
			{
				return category;
			}
			else
			{
				category = Sungero.Contracts.ContractCategories.Create();
				category.Name = Category;
				category.Save();
				
				return category;
			}
		}
		
		/// <summary>
		/// Создание или обновление документа Акта выполненных работ в системе Парус 8
		/// </summary>
		/// <param name="Document"></param>
		[Remote, Public]
		public static void Sync_ContractStatement_Parus8(IContractStatement Document)
		{
			var documentRN = string.Empty;
			var contractRN = string.Empty;
			var counterpartyRN = string.Empty;
			var businessUnitRN = string.Empty;
			
			if (!TryRN_By_ID(Document.LeadingDocument.Id,out contractRN))
				contractRN = Sync_Contract_Parus8(Document.LeadingDocument);
			
			if (!TryRN_By_ID(Document.Counterparty.Id, out counterpartyRN))
				counterpartyRN = Sync_Organization_Parus8(Document.Counterparty);
			
			if (!TryRN_By_ID(Document.BusinessUnit.Id, out businessUnitRN))
				businessUnitRN  = Sync_Organization_Parus8(Document.BusinessUnit.Company);
			
			OracleConnection connection = new OracleConnection();
			connection.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.69)(PORT=1521)))(CONNECT_DATA=(SID=kse)));User Id=directum;Password=$$$$$$aP@;";
			connection.Open();

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandType = System.Data.CommandType.StoredProcedure;
				command.CommandText = "XXX_XXX";
				command.Parameters.Add("sParam1", Document.RegistrationNumber);
				command.Parameters.Add("sParam2", ((DateTime)Document.RegistrationDate).ToString("d"));
//				command.Parameters.Add("sParam2", OracleDbType.Date, Document.RegistrationDate, ParameterDirection.Input);
				command.Parameters.Add("sParam3", counterpartyRN);
				command.Parameters.Add("sParam4", businessUnitRN);
				command.Parameters.Add("sParam5", contractRN);
				command.Parameters.Add("sParam6", Document.TotalAmount.ToString());
				command.Parameters.Add("nRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
				command.ExecuteNonQuery();
				
				documentRN = command.Parameters["nRN"].Value.ToString();
				
				Sync_ExternalEntityLink(Document.Id, "26fd4043-40d2-457c-ad0e-35cefa05d8dd", documentRN, "Акт выполненных работ");
			}
		}
		
		/// <summary>
		/// Создание или обновление документа входящего счета на оплату в системе Парус 8
		/// </summary>
		/// <param name="Document"></param>
		[Remote, Public]
		public static void Sync_Waybill_Parus8(IWaybill Document)
		{
			var documentRN = string.Empty;
			var contractRN = string.Empty;
			var counterpartyRN = string.Empty;
			var BusinessUnitRN = string.Empty;
			
			if (!TryRN_By_ID(Document.LeadingDocument.Id,out contractRN))
				contractRN = Sync_Contract_Parus8(Document.LeadingDocument);
			
			if (!TryRN_By_ID(Document.Counterparty.Id, out counterpartyRN))
				counterpartyRN = Sync_Organization_Parus8(Document.Counterparty);
			
			if (!TryRN_By_ID(Document.BusinessUnit.Id, out BusinessUnitRN))
				BusinessUnitRN  = Sync_Organization_Parus8(Document.BusinessUnit.Company);
			
//			Document.Id;
//			Document.RegistrationNumber;
//			Document.RegistrationDate.ToString("d");
//			Document.Counterparty;
//			Document.BusinessUnit;
//			Document.LeadingDocument;
			
			Sync_ExternalEntityLink(Document.Id, "167d0859-1aa3-4d7b-934a-bfc4b0a8c7a1", documentRN, "Накладная");
		}
		
		/// <summary>
		/// Создание или обновление документа входящего счета на оплату в системе Парус 8
		/// </summary>
		/// <param name="Document"></param>
		[Remote, Public]
		public static void Sync_UniversalTransferDocument_Parus8(IUniversalTransferDocument Document)
		{
			var documentRN = string.Empty;
			var contractRN = string.Empty;
			var counterpartyRN = string.Empty;
			var BusinessUnitRN = string.Empty;
			
			if (!TryRN_By_ID(Document.LeadingDocument.Id,out contractRN))
				contractRN = Sync_Contract_Parus8(Document.LeadingDocument);
			
			if (!TryRN_By_ID(Document.Counterparty.Id, out counterpartyRN))
				counterpartyRN = Sync_Organization_Parus8(Document.Counterparty);
			
			if (!TryRN_By_ID(Document.BusinessUnit.Id, out BusinessUnitRN))
				BusinessUnitRN  = Sync_Organization_Parus8(Document.BusinessUnit.Company);
			
//			Document.Id;
//			Document.RegistrationNumber;
//			Document.RegistrationDate.ToString("d");
//			Document.Counterparty;
//			Document.BusinessUnit;
//			Document.LeadingDocument;
			
			Sync_ExternalEntityLink(Document.Id, "bc6b4e2e-6299-4508-ad08-8745ab02ae8a", documentRN, "Универсальный передаточный документ");
		}
		
	}
}