﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using System.IO;
using ProgressBarLibrary;


namespace syscon.Parus8.Client
{
	public class ModuleFunctions
	{

		/// <summary>
		/// 
		/// </summary>
		public virtual void Function5()
		{
			syscon.Parus8.Functions.Module.Remote.ImportContractCatFromExcel();
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void Sync_Organizations_RX()
		{
			var cntLoad = 0;
			var cntError = 0;
			var cntAll = 0;
			
			var logsFolder = @"C:\DRX\logs\";

			if (!Directory.Exists(logsFolder))
				Directory.CreateDirectory(logsFolder);
			
			var listOrgRN = PublicFunctions.Module.Remote.GetChangedObjects_Parus8(1);
			
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			
			ProgressBarDRX progressBar = new ProgressBarDRX(listOrgRN.Count); //объект прогресс бар
			progressBar.Show();
			progressBar.Text("Подключение к базе...");
			
			foreach(var orgRN in listOrgRN)
			{
				try
				{
					PublicFunctions.Module.Remote.Sync_Organizations_RX(orgRN);

					using (var file = File.AppendText(string.Format(logsFolder + "log_migration_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Организация c RN {0} загружена.", orgRN));
					}

					cntLoad++;
				}
				catch (Exception ex)
				{
					using (var file = File.AppendText(string.Format(logsFolder + "log_error_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Организация c RN {0} не загружена.\r\n{1}", orgRN, ex.Message));
					}

//								ListOrgRN.Remove(reader["RN"].ToString());
					cntError++;
				}
				cntAll++;
				
				progressBar.Text(string.Format("Загрузка организаций: {0}/{1}...\r\nКол-во ошибок: {2}.", cntLoad, listOrgRN.Count, cntError));
				progressBar.Next();
			}
			
			progressBar.Hide();
			progressBar = null;

			sw.Stop();
			
			//				RemoveChangedObject_Parus8(ListOrgRN);

			Sungero.Core.Dialogs.ShowMessage(string.Format("Загрузка завершена.\r\n"
			                                               + "---------------------------------------------------------\r\n"
			                                               + "Количество загруженных организаций: {0}\r\n"
			                                               + "Количество ошибок при загрузке: {1}\r\n"
			                                               + "---------------------------------------------------------\r\n"
			                                               + "Количество просмотренных организаций: {2}\r\n"
			                                               + "---------------------------------------------------------\r\n"
			                                               + "Время выполнения: {3}",
			                                               cntLoad, cntError, cntAll,
			                                               sw.Elapsed.ToString(@"d\.hh\:mm\:ss")));
			
			sw = null;
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void Function4()
		{
//			string val = string.Empty;
//			Function44("123", out val);
//			Dialogs.ShowMessage(val);
		}
		
		/// <summary>
		/// 
		/// </summary>
		private static bool Function44(string t, out string b)
		{
			b = t;
			
			return true;
		}
		
		public virtual System.Collections.Generic.Dictionary<string, string> ret_dic(string val1)
		{
			val1 = "****";
			Dictionary<string, string> dic = new Dictionary<string, string>();
			dic.Add("1","привет");
			dic.Add("2","пока");
			return dic;
		}
		/// <summary>
		/// 
		/// </summary>
		public virtual void Function3()
		{
			//Functions.Module.Remote.Create_category("Договор Аренды").Show();
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void Function2()
		{
			var dlg = Sungero.Core.Dialogs.CreateInputDialog("Запрос параметров");
			var p1 = dlg.AddString("RN",true,"101");
			var p2 = dlg.AddString("Категория",true,"Договор Аренды");
			var p3 = dlg.AddString("Рег Номер",true,"123");
			var p4 = dlg.AddString("Дата документа",true,"06.12.2009");
			var p5 = dlg.AddString("Примечание",true,"договор");
			var p6 = dlg.AddString("Сумма",true,"1250,50");
			var p7 = dlg.AddString("Дата с",true,"01.01.2018");
			var p8 = dlg.AddString("Дата по",true,"31.12.2018");
			var p9 = dlg.AddString("RN орг",true,"3");
			var p10 = dlg.AddString("RN наш орг",true,"2");
			var p11 = dlg.AddString("ИНН подписанта",true,"482608013231");
			var p12 = dlg.AddString("ИНН ответственного",true,"482608013231");
			var p13 = dlg.AddString("Ссылка на файл",true,"C:\\123.txt");
			var p14 = dlg.AddString("Открыть карточку?",true, "true");
			if (dlg.Show() == DialogButtons.Ok)
			{
				Create_Contract_RX(p1.Value,p2.Value,p3.Value,p4.Value,p5.Value, p6.Value, p7.Value, p8.Value, p9.Value, p10.Value, p11.Value, p12.Value, p13.Value,p14.Value);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void Function1()
		{
			var dlg = Sungero.Core.Dialogs.CreateInputDialog("Запрос параметров");
			var p1 = dlg.AddString("RN",true,"2");
			var p2 = dlg.AddString("ORG",true,"Org");
			var p3 = dlg.AddString("INN",true,"7811417616");
			var p4 = dlg.AddString("KPP",true,"781301001");
			var p5 = dlg.AddBoolean("Открыть карточку?",true);
			if (dlg.Show() == DialogButtons.Ok)
			{
				Create_ORG_RX(p1.Value,p2.Value,p3.Value,p4.Value,"true");
			}
		}
		
		[Hyperlink]
		public static void Create_ORG_RX(string RN, string Name, string INN, string KPP, string NeedOpenCard)
		{
			var org = syscon.Parus8.Functions.Module.Remote.Sync_Organization_RX(RN, Name, INN, KPP);
			if (org != null && NeedOpenCard == "true")
			{
				org.Show();
			}
		}
		
		[Hyperlink]
		public static void Create_Contract_RX(string RN, string Category, string RegNum, string DateDoc, string Note, string Sum, string DateFrom, string DateTo, string ORG, string OurOrg, string Signer, string Responsible, string File, string NeedOpenCard)
		{

			var  contract = syscon.Parus8.Functions.Module.Remote.Sync_Contract_RX(RN, Category, RegNum, DateDoc, Note, Sum, DateFrom, DateTo, ORG, OurOrg, Signer, Responsible, File);
			if (contract != null && NeedOpenCard == "true")
			{
				contract.Show();
			}
		}
		
		[Hyperlink]
		public static void Create_IncomingInvoice_RX(string RN, string Category, string RegNum, string DateDoc, string Note, string Sum, string DateFrom, string DateTo, string ORG, string OurOrg, string Signer, string Responsible, string File, string NeedOpenCard)
		{

			var  contract = syscon.Parus8.Functions.Module.Remote.Sync_Contract_RX(RN, Category, RegNum, DateDoc, Note, Sum, DateFrom, DateTo, ORG, OurOrg, Signer, Responsible, File);
			if (contract != null && NeedOpenCard == "true")
			{
				contract.Show();
			}
		}
		
		public static void OpenREf()
		{
			var dlg = Sungero.Core.Dialogs.CreateInputDialog("hallo");
			dlg.Show();
			
		}
		
//		[Hyperlink]
//		public static void Create_IncomingInvoice_RX(string RN, string Category, string RegNum, string DateDoc, string Note, string Sum, string DateFrom, string DateTo, string ORG, string OurOrg, string Signer, string Responsible, string File, string NeedOpenCard)
//		{
//
//			var  contract = syscon.Parus8.Functions.Module.Remote.Create_Contract_RX(RN, Category, RegNum, DateDoc, Note, Sum, DateFrom, DateTo, ORG, OurOrg, Signer, Responsible, File);
//			if (contract != null && NeedOpenCard == "true")
//			{
//				contract.Show();
//			}
//		}

	}
}