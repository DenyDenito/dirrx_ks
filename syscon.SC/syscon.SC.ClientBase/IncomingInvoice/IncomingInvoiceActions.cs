﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.IncomingInvoice;

namespace syscon.SC.Client
{
	partial class IncomingInvoiceActions
	{
		public virtual void SynchronizeParus8KS(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			_obj.Save();
			
			Parus8.PublicFunctions.Module.Remote.Sync_IncomingInvoice_Parus8(_obj);
		}

		public virtual bool CanSynchronizeParus8KS(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

	}

}