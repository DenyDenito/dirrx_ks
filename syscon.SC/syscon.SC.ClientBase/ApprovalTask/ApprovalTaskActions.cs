﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.ApprovalTask;

namespace syscon.SC.Client
{
	partial class ApprovalTaskActions
	{
		public override void Start(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			bool chek = false;
			foreach (var role in _obj.RolesSysconsyscon)
			{
				if (role.Required == true && role.EmpProperty == null)
				{
					e.AddError("Не указан исполнитель роли \"" + role.RoleProperty.DisplayValue + " \"");
					chek = true;
				}
				
			}
			if (!chek)
				base.Start(e);
		}

		public override bool CanStart(Sungero.Domain.Client.CanExecuteActionArgs e)
		{

			
			return base.CanStart(e);
		}

	}

}