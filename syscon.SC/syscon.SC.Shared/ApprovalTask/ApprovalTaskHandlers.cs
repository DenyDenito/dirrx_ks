﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.ApprovalTask;

namespace syscon.SC
{
	partial class ApprovalTaskRolesSysconsysconSharedHandlers
	{

		public virtual void RolesSysconsysconEmpPropertyChanged(syscon.SC.Shared.ApprovalTaskRolesSysconsysconEmpPropertyChangedEventArgs e)
		{
			var task = _obj.ApprovalTask;
			task.State.Controls.Control.Refresh();
		}
	}

	partial class ApprovalTaskSharedHandlers
	{

		public override void ApprovalRuleChanged(Sungero.Docflow.Shared.ApprovalTaskApprovalRuleChangedEventArgs e)
		{
			var Rule = e.NewValue;
			if (Rule != null)
			{
				_obj.RolesSysconsyscon.Clear();
				foreach (var stage in Rule.Stages)
				{
					var stage_sc = syscon.SC.ApprovalStages.As(stage.Stage);
					if (stage_sc != null && stage_sc.FilledByUsersyscon == true)
					{
						if (stage_sc.ApprovalRoles.Count >0)
						{
							foreach (var role in stage_sc.ApprovalRoles)
							{
								var s = _obj.RolesSysconsyscon.AddNew();
								s.RoleProperty = role.ApprovalRole;
								if (stage_sc.Requiredsyscon != null)
									s.Required = stage_sc.Requiredsyscon;
								else
									s.Required = false;
							}
						}
						else
						{
							if (stage_sc.ApprovalRole != null)
							{
								var s = _obj.RolesSysconsyscon.AddNew();
								s.RoleProperty = stage_sc.ApprovalRole;
								if (stage_sc.Requiredsyscon != null)
									s.Required = stage_sc.Requiredsyscon;
								else
									s.Required = false;
							}
						}
					}
				}
			}
			base.ApprovalRuleChanged(e);
		}

		public virtual void RegGroupKSChanged(syscon.SC.Shared.ApprovalTaskRegGroupKSChangedEventArgs e)
		{
			var regGroup = RegistrationGroups.As(_obj.RegGroupKS);
			
			if (regGroup != null)
			{
				foreach (var role in _obj.RolesSysconsyscon)
				{
					foreach (var roleRegGroup in regGroup.Rolessyscon)
					{
						if (Equals(role.RoleProperty,roleRegGroup.Role))
						{
							role.EmpProperty = roleRegGroup.EmpRole;
						}
					}
				}
				
			}
		}

	}
}