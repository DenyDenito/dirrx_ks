﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.ApprovalStage;

namespace syscon.SC.Shared
{
	partial class ApprovalStageFunctions
	{
		public override List<Enumeration?> GetPossibleRoles()
		{
			var baseRoles = base.GetPossibleRoles();
			if (_obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Approvers ||
			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.SimpleAgr ||
			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Notice)
			{
				baseRoles.Add(syscon.Parus8.Role.Type.DivisionHead);
				baseRoles.Add(syscon.Parus8.Role.Type.LegalDep);
				baseRoles.Add(syscon.Parus8.Role.Type.PersonnelDel);
				baseRoles.Add(syscon.Parus8.Role.Type.Accountant);
				baseRoles.Add(syscon.Parus8.Role.Type.HeadSecurity);
				baseRoles.Add(syscon.Parus8.Role.Type.FinHead);
			}
			
			if (_obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Register)
			{
				baseRoles.Add(syscon.Parus8.Role.Type.PersonnelDel);
			}
			return baseRoles;
		}
	}
}